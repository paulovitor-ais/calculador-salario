package br.com.paulovitor.calculadorsalario.domain;

import static br.com.paulovitor.calculadorsalario.domain.Cargo.DBA;
import static br.com.paulovitor.calculadorsalario.domain.Cargo.DESENVOLVEDOR;
import static br.com.paulovitor.calculadorsalario.domain.Cargo.TESTER;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CalculadoraDeSalarioTest {

	private CalculadoraDeSalario calculadoraDeSalario;
	private Calendar hoje;

	@Before
	public void setUp() {
		calculadoraDeSalario = new CalculadoraDeSalario();
		hoje = Calendar.getInstance();
	}

	@Test
	public void deveCalculaSalarioDeUmDesenvolvedor() {
		// give
		Funcionario funcionario = new Funcionario("João", DESENVOLVEDOR, hoje, 3000);

		// when
		double salario = calculadoraDeSalario.calcula(funcionario);

		// then
		assertThat(2700.0, is(salario));
	}

	@Test
	public void deveCalculaSalarioDeUmDBA() {
		// give
		Funcionario funcionario = new Funcionario("Fernando", DBA, hoje, 3500);

		// when
		double salario = calculadoraDeSalario.calcula(funcionario);

		// then
		assertThat(2625.0, is(salario));
	}

	@Test
	public void deveCalculaSalarioDeUmTester() {
		// give
		Funcionario funcionario = new Funcionario("José", TESTER, hoje, 2000);

		// when
		double salario = calculadoraDeSalario.calcula(funcionario);

		// then
		assertThat(1700.0, is(salario));
	}

}
