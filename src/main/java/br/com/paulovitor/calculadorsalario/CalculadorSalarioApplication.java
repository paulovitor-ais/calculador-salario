package br.com.paulovitor.calculadorsalario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculadorSalarioApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculadorSalarioApplication.class, args);
	}
}
