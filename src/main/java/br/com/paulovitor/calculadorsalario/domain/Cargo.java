package br.com.paulovitor.calculadorsalario.domain;

public enum Cargo {

    DESENVOLVEDOR, DBA, TESTER
}
