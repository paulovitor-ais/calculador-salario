package br.com.paulovitor.calculadorsalario.domain;

import java.util.Calendar;

public class Funcionario {

    private Cargo cargo;
    private double salarioBase;

    public Funcionario(String nome, Cargo cargo, Calendar admissao, double salarioBase) {
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public double getSalarioBase() {
        return salarioBase;
    }

    public void setSalarioBase(double salarioBase) {
        this.salarioBase = salarioBase;
    }
}
