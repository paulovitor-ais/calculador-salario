package br.com.paulovitor.calculadorsalario.domain;

public class ProcessadorDeInvestimentos {

    public static void main(String[] args) throws Throwable {

        for (ContaComum conta : contasDoBanco()) {

            conta.rende();

            System.out.println("Novo saldo");
            System.out.println(conta.getSaldo());
        }
    }

    private static ContaComum[] contasDoBanco() {
        return new ContaComum[] { new ContaComum(), new ContaDeEstudante() };
    }
}
