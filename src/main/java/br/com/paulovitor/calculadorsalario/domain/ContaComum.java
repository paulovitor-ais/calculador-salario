package br.com.paulovitor.calculadorsalario.domain;

public class ContaComum {

    protected double saldo;

    public ContaComum() {
        this.saldo = 0;
    }

    public void deposita(double valor) throws ValorInvalidoException {
        if (valor <= 0)
            throw new ValorInvalidoException();

        this.saldo += valor;
    }

    public void rende() throws Throwable {
        this.saldo *= 1.1;
    }

    public double getSaldo() {
        return saldo;
    }
}
