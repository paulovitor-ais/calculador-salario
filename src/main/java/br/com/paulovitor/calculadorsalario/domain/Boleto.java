package br.com.paulovitor.calculadorsalario.domain;

public class Boleto {

    private double valor;

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
